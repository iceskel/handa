import { generateCreateActions } from 'utils/redux';

export const parseKanji = 'parse_kanji';

export const canvas = {
  ...generateCreateActions(parseKanji),
};
