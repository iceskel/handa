import camelCase from 'lodash.camelcase';

export function createAction(type, ...argNames) {
  return function(...args) {
    const action = { type };
    argNames.forEach((arg, index) => {
      action[argNames[index]] = args[index];
    });
    return action;
  };
}

export function createReducer(initialState, actionHandlers) {
  return function reducer(state = initialState, action) {
    if (actionHandlers.hasOwnProperty(action.type)) {
      return actionHandlers[action.type](state, action);
    } else {
      return state;
    }
  };
}

export function generateCreateActions(key) {
  return {
    [`${camelCase(key)}Request`]: createAction(`${key.toUpperCase()}_REQUEST`),
    [`${camelCase(key)}Success`]: createAction(
      `${key.toUpperCase()}_SUCCESS`,
      'payload'
    ),
    [`${camelCase(key)}Failure`]: createAction(
      `${key.toUpperCase()}_FAILURE`,
      'error'
    ),
  };
}

export function generateActionTypes(key) {
  return {
    [`${key.toUpperCase()}_REQUEST`]: `${key.toUpperCase()}_REQUEST`,
    [`${key.toUpperCase()}_SUCCESS`]: `${key.toUpperCase()}_SUCCESS`,
    [`${key.toUpperCase()}_FAILURE`]: `${key.toUpperCase()}_FAILURE`,
  };
}

export function generateReducers(...keys) {
  const obj = {};
  for (const key of keys) {
    obj[`${key.type.toUpperCase()}_REQUEST`] = (state, action) =>
      Object.assign({}, state, {
        pending: true,
        ...(key.body && key.body.request && key.body.request(state, action)),
      });
    obj[`${key.type.toUpperCase()}_SUCCESS`] = (state, action) =>
      Object.assign({}, state, {
        ...action,
        pending: false,
        ...(key.body && key.body.success && key.body.success(state, action)),
      });
    obj[`${key.type.toUpperCase()}_FAILURE`] = (state, action) =>
      Object.assign({}, state, {
        ...action,
        pending: false,
        ...(key.body && key.body.failure && key.body.failure(state, action)),
      });
  }
  return obj;
}
