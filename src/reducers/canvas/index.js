import initialState from './initialState';
import { parseKanji } from 'actions/canvas';
import { generateReducers, createReducer } from 'utils/redux';

const actionHandlers = {
  ...generateReducers(
    { type: parseKanji },
  ),
};

export default createReducer(initialState, actionHandlers);
