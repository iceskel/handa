import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from 'reducers';
import createHistory from 'history/createHashHistory';
import { connectRouter, routerMiddleware } from 'connected-react-router';

export const history = createHistory();

const appRouterMiddleware = routerMiddleware(history);

const store = createStore(
  connectRouter(history)(rootReducer),
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
  compose(
    applyMiddleware(thunk),
    applyMiddleware(appRouterMiddleware)
  )
);

export default store;
