/* istanbul ignore file */
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import React from 'react';
import ReactDOM from 'react-dom';
import { injectGlobal } from 'emotion';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';

import App from './App';
import registerServiceWorker from './registerServiceWorker';
import store, { history } from './store';

injectGlobal`
  * {
    box-sizing: border-box;
  }
  body {
      margin: 0;
      padding: 0;
      background-color: lightgray;
  }  
`;

ReactDOM.render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <App />
    </ConnectedRouter>
  </Provider>,
  document.getElementById('root')
);
registerServiceWorker();
