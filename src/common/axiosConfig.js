/* istanbul ignore file */

import axios from 'axios';

axios.defaults.baseURL = `${process.env.REACT_APP_BASE_URL}`;

export function setCredentials(token) {
  if (token) {
    axios.defaults.headers = {
      Authorization: `Bearer ${token}`,
    };
  } else {
    delete axios.defaults.headers;
  }
}
