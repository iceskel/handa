import { combineReducers } from 'redux';

import canvasReducer from './canvas';

export default combineReducers({
  canvas: canvasReducer,
});
