import { connect } from 'react-redux';

import { Canvas } from './Canvas';

const mapStateToProps = state => ({
  ...state,
});

export default connect(
  mapStateToProps
)(Canvas);
