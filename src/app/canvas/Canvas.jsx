import React, { Component, Fragment } from 'react';
import SvgSketchCanvas from 'react-sketch-canvas';

const styles = {
  border: '0.0625rem solid #9c9c9c',
  borderRadius: '0.25rem',
};

export class Canvas extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.canvas = null;
  }

  printIt = async () => {
    const paths = await this.canvas.exportPaths();
    console.log(paths);
  };

  clear = () => {
    this.canvas.clearCanvas();
  };

  render() {
    return (
      <Fragment>
        <SvgSketchCanvas
          ref={element => {
            this.canvas = element;
          }}
          width="300px"
          height="300px"
          styles={styles}
          strokeWidth={3}
          strokeColor="black"
        />
        <button
          type="button"
          className="btn btn-primary"
          onClick={this.printIt}
        >
          Log
        </button>
        <button type="button" className="btn" onClick={this.clear}>
          Clear
        </button>
      </Fragment>
    );
  }
}
