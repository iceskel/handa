/* istanbul ignore file */

import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import intl from 'react-intl-universal';

import Canvas from 'app/canvas';

const mapStateToProps = state => ({
  ...state,
});

const locales = {
  'en-US': require('locales/en-US.json'),
};

class App extends Component {
  state = {
    initDone: false,
  };

  componentDidMount() {
    this.loadLocales();
  }

  loadLocales = () => {
    intl
      .init({
        currentLocale: 'en-US',
        locales,
      })
      .then(() => {
        this.setState({
          initDone: true,
        });
      });
  };

  render() {
    return (
      <div className="App container">
        <Helmet>
          <meta charSet="utf-8" />
          <title>Handa</title>
        </Helmet>

        <Route exact path="/" render={props => <Canvas {...props} />} />
      </div>
    );
  }
}

export default connect(mapStateToProps)(App);
